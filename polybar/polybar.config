;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
background = ${xrdb:color0:#222}
foreground = ${xrdb:color7:#222}
foreground-alt = ${xrdb:color7:#222}
primary = ${xrdb:color1:#222}
secondary = ${xrdb:color2:#222}
alert = ${xrdb:color3:#222}
extra = ${xrdb:color4:#222}

[bar/nord-works]
monitor = ${env:MONITOR:eDP1}
width = 150
height = 30
offset-x = 1%
offset-y = 1%
bottom = true

background = ${colors.background}
foreground = ${colors.foreground}

font-0 = "monospace:pixelsize=16;0"

modules-left = 
modules-center = ewmh-nord-first
modules-right = 

;override-redirect = true

cursor-click = pointer
cursor-scroll = ns-resize

[bar/nord-infos]
monitor = ${env:MONITOR:eDP1}
width = 18%
height = 30
offset-x = 81%
offset-y = 1%
bottom = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = ${colors.extra}

padding-left = 0
padding-right = 2

module-margin-left = 0
module-margin-right = 2

font-0 = "MesloLGS NF:pixelsize=16;0"

modules-left = 
modules-center = wlan battery date
modules-right = 

override-redirect = true

cursor-click = pointer
cursor-scroll = ns-resize

[bar/nord]
monitor = ${env:MONITOR:eDP1}
width = 36%
height = 30
offset-x = 32%
offset-y = 1%
;radius = 0.0
bottom = true
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = ${colors.foreground}

border-size = 0
border-color = ${colors.foreground}

padding-left = 0
padding-right = 0

module-margin-left = 0
module-margin-right = 2

font-0 = "MesloLGS NF:size=14:weight=bold;0"

modules-left = mymenu
modules-center = backlight-acpi alsa
modules-right = powermenu batterycheck

tray-position = right
tray-padding = 2
tray-background = ${colors.primary}

override-redirect = true

cursor-click = pointer
cursor-scroll = ns-resize

[module/title]
type = internal/xwindow

format = <label>
format-background = ${colors.extra}
format-foreground = ${colors.background}
format-padding = 1

label = %title%
label-maxlen = 20

;label-empty = Empty
;label-empty-foreground = ${colors.background}

[module/ewmh-nord-second]
inherit = module/ewmh-nord
icon-5 = 6;6
icon-6 = 7;7
icon-7 = 8;8
icon-8 = 9;9

[module/ewmh-nord-first]
inherit = module/ewmh-nord

icon-0 = 1;1
icon-1 = 2;2
icon-2 = 3;3
icon-3 = 4;4
icon-4 = 5;5

[module/ewmh-nord]
type = internal/xworkspaces

;pin-workspaces = true
enable-click = true
enable-scroll = true

format = <label-state>

label-monitor = %name%
label-occupied = %icon%
label-active = %icon%
label-empty = %icon% 
label-active-foreground = ${colors.background}
label-active-background = ${colors.extra}
label-occupied-foreground = ${colors.extra}
label-occupied-background = ${colors.background}
label-empty-foreground = ${colors.foreground}
label-empty-background = ${colors.background}
label-empty-padding = 1
label-occupied-padding = 1
label-active-padding = 1

;一 二 三 四 五 六 七 八 九 十

;icon-default = 

[module/ewmh]
type = internal/xworkspaces

;pin-workspaces = true
enable-click = true
enable-scroll = true

format = <label-state>

label-monitor = %name%
label-occupied = 
label-active = 
label-empty = %icon% 
label-active-foreground = ${colors.background}
label-active-background = ${colors.foreground}
label-occupied-foreground = ${colors.background}
label-occupied-background = ${colors.foreground}
label-empty-foreground = ${colors.background}
label-empty-background = ${colors.foreground}
label-empty-padding = 1
label-occupied-padding = 1
label-active-padding = 1

;一 二 三 四 五 六 七 八 九 十

icon-0 = 1;
icon-1 = 2;
icon-2 = 3;
icon-3 = 4;
icon-4 = 5;
icon-5 = 6;
icon-6 = 7;
icon-7 = 8;
icon-8 = 9;
icon-default = 

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted = : %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/backlight-acpi]
type = internal/backlight
card = intel_backlight
enable-scroll = true
use-actual-brightness = true

format = <label> <bar>
label = ﯦ %percentage%%

bar-width = 10
bar-indicator = 
bar-indicator-foreground = ${colors.primary}
bar-indicator-font = 0
bar-fill = 
bar-fill-font = 0
bar-fill-foreground = ${colors.secondary}
bar-empty = 
bar-empty-font = 0
bar-empty-foreground = ${colors.foreground}

[module/wlan]
type = internal/network
interface = wlan0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = ${colors.extra}
label-connected = 
label-connected-font = 0

format-disconnected = <label-disconnected>
format-disconnected-underline = ${self.format-connected-underline}
label-disconnected = 
label-disconnected-font = 0
label-disconnected-foreground = ${colors.foreground}

ramp-signal-0 = 睊
ramp-signal-1 = 直
ramp-signal-2 = 
ramp-signal-font = 0
ramp-signal-foreground = ${colors.foreground}

[module/eth]
type = internal/network
interface = enp2s0f1
interval = 3.0

format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 5

date =
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.extra}

label = %date% %time%

[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = 墳 %percentage%%
label-volume-font = 0
label-volume-foreground = ${colors.foreground}

format-muted-foreground = ${colors.foreground}
label-muted = 婢 Muted
label-muted-font = 0

bar-volume-width = 10
bar-volume-indicator = 
bar-volume-indicator-foreground = ${colors.secondary}
bar-volume-indicator-font = 2
bar-volume-fill = 
bar-volume-fill-font = 2
bar-volume-fill-foreground = ${colors.primary}
bar-volume-empty = 
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground}

[module/battery]
type = internal/battery
battery = BAT1
adapter = ACAD
full-at = 98

format-charging = <animation-charging> <label-charging>
format-charging-underline = ${colors.extra}

format-discharging = <animation-discharging> <label-discharging>
format-discharging-underline = ${colors.extra}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-5 = 
ramp-capacity-6 = 
ramp-capacity-7 = 
ramp-capacity-8 = 
ramp-capacity-9 = 
ramp-capacity-foreground = ${colors.foreground}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-5 = 
animation-charging-6 = 
animation-charging-7 = 
animation-charging-8 = 
animation-charging-9 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 950

animation-discharging-9 = 
animation-discharging-8 = 
animation-discharging-7 = 
animation-discharging-6 = 
animation-discharging-5 = 
animation-discharging-4 = 
animation-discharging-3 = 
animation-discharging-2 = 
animation-discharging-1 = 
animation-discharging-0 = 
animation-discharging-foreground = ${colors.foreground}
animation-discharging-framerate = 950

[module/mymenu]
type = custom/text
content = 
content-foreground = ${colors.background}
content-background = ${colors.foreground}
content-padding = 1

click-left = $HOME/scripts/appmenu
click-right = $HOME/scripts/appimg-launcher

[module/powermenu]
type = custom/text
content = ⏻
content-foreground = ${colors.background}
content-background = ${colors.foreground}
content-padding = 1

click-left = $HOME/scripts/powermenu

[module/batterycheck]
type = custom/script
exec = $HOME/scripts/battery-check
format = <label>
label = 

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini
